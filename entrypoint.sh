#!/bin/bash
sleep 1

cd /home/baard

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/baard$ ${MODIFIED_STARTUP}"

# Run the Server
${MODIFIED_STARTUP}