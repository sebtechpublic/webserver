#!/bin/ash
rm -rf /home/baard/tmp/*

echo "Starting PHP"
/usr/sbin/php-fpm8 --fpm-config /home/baard/php-fpm/php-fpm.conf --daemonize

echo "Starting Nginx"
echo "Success"
/usr/sbin/nginx -c /home/baard/nginx/nginx.conf -p /home/baard/